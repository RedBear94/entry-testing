function fib(N) {
	let f1 = 1, f2 = 1;
  
	if((N==1) || (N==2))
	{
		f2=1;
	}
	else if(N>2)
	{
	  for (let i = 3; i <= N; i++) {
		let F = f1 + f2;
		f1 = f2;
		f2 = F;
	  }
	}
	return f2;
}

console.log( fib(10) ); // 55
console.log( fib(8) ); // 21